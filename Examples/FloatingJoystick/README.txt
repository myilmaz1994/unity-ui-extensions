Getting started with a floating joystick

1. Open a FloatingJoystick scene
2. Import Unity Standard Assets/Cross Platfrom Input
- Assets -> Import Package -> CrossPlatformInput
3. Add scripting define symbols
- Edit -> Project Settings -> Player -> Other Settings -> Scripting Define symbols
- Add "CROSS_PLATFORM_INPUT;MOBILE_INPUT" (probably, there will be only "CROSS_PLATFORM_INPUT")
- Press Enter to submit changes (!important)
4. Play the scene